<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\User;


class UserController extends BaseController
{
    public function index()
    {
        $data = [];
        helper(['form']);

        if ($this->request->getMethod() == 'post') {
            //let's do the validation here
            $rules = [
                'nid' => 'required',
                'password' => 'required|validateUser[nid,password]',
            ];

            $errors = [
                'password' => [
                    'validateUser' => 'Nid or Password don\'t match'
                ]
            ];

            if (! $this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            }else{
                $auth = new User();
                $user = $auth->where('nid', $this->request->getVar('nid'))->first();
                $this->setUserSession($user);
                return redirect()->to('letters');
            }
        }
        return view('login/login', $data);
    }



    private function setUserSession($user)
    {
        $data = [
            'userid' => $user['id'],
            'nid' => $user['nid'],
            'username' => $user['name'],
            'sex' => $user['sex'],
            'contact' => $user['contact'],
            'level_id' => $user['level_id'],
            'verify_id' => $user['verify_id'],
            'pass' => $user['password'],
            'isLoggedIn' => true,
        ];

        session()->set($data);
        return true;
    }



    public function userProfile($id)
    {
        helper(['form', 'url']);

        $user = new User();
        $users = $user->userProfile($id);
        $data['usersProfile'] = $users;

        if($users['id'] != session()->get('userid')){
            return redirect()->to(base_url('403'));
        }

        return view('users/profile',$data);
    }


    public function avatar()
    {
        helper(['form', 'url']);
        $data = [];
        $this->UserModel = new User();
        if ($this->request->getMethod() == 'post'){
            $rules = ['avatar' => 'uploaded[avatar]|max_size[avatar,1024]|ext_in[avatar,png,jpg]'];
            if($this->validate($rules)){
                $file = $this->request->getFile('avatar');
                if($file->isValid() && !$file->hasMoved()){
                    if($file->move(FCPATH. '/assets/profiles', $file->getRandomName()))
                    {
                        $path = base_url() . '/assets/profiles/' . $file->getName();
                        $status =  $this->UserModel->updateAvatar($path, session()->get('userid') );

                        if($status == true){
                            session()->setTempdata('success','Avatar Uploaded', 3);
                            return redirect()->to(base_url('profile/'. session()->get('userid')));
                        } else {
                            session()->setTempdata('error','Unable to Upload Avatar', 3);
                            return redirect()->to(base_url('profile/'. session()->get('userid')));
                        }
                    } else {
                        session()->setTempdata('error',$file->getErrorString(), 3);
                        return redirect()->to(base_url('profile/'. session()->get('userid')));
                    }
                } else {
                    session()->setTempdata('error','Invalid File', 3);
                    return redirect()->to(base_url('profile/'. session()->get('userid')));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }

        return redirect()->to(base_url('profile/'. session()->get('userid')));
    }



    public function removeAvatar($id)
    {
        helper(['url']);
        $this->UserModel = new User();
        $this->UserModel->remAvatar($id);
        return redirect()->to(base_url('profile/'. session()->get('userid')));
    }


    public function changePassword()
    {
        helper(['form', 'url']);

        $this->UserModel = new User();

        if ($this->request->getMethod() == 'post')
        {
            $rules = ['opwd' => 'required', 'npwd' => 'required|min_length[4]|max_length[10]', 'cnpwd' => 'required|matches[npwd]'];

            if($this->validate($rules)){
                $opwd = $this->request->getVar('opwd');
                $npwd = password_hash($this->request->getVar('npwd'), PASSWORD_DEFAULT);

                if (password_verify($opwd, session()->get('pass'))){
                    if($this->UserModel->updatePassword($npwd,session()->get('userid'))){
                        session()->setFlashdata('success','Password Changed');
                        return redirect()->to(base_url('profile/'. session()->get('userid')));
                    } else {
                        session()->setFlashdata('error','Unable to update the password');
                        return redirect()->to(base_url('profile/'. session()->get('userid')));
                    }
                } else {
                    session()->setFlashdata('error','Unable to update the password');
                    return redirect()->to(base_url('profile/'. session()->get('userid')));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }

        return redirect()->to(base_url('profile/'. session()->get('userid')));
    }


    public function logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }


}