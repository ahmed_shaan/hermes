<?php

namespace App\Controllers;

use App\Helpers\Enums\StatusColors;
use App\Models\Message;
use App\Models\User;
use App\Models\Letter;


class LetterController extends BaseController
{
    public function index()
	{
        $signers = new User();

        //this is for displaying data to the dashboard
        $filter_data = [
            'status' => $this->request->getVar('status'),
            'supervisor' => $this->request->getVar('supervisor'),
            'assign_to' => $this->request->getVar('assign_to'),
            'search' => $this->request->getVar('search'),
            'from' => $this->request->getVar('from'),
            'to' => $this->request->getVar('to')
        ];



        $letters = new Letter();

        $user_id = session()->get('userid');
        $user_level = session()->get('level_id');

        //Filter Letters in Dashboard
        if($user_level == 4){
            $letters = $letters->where('user_id', $user_id);
        }
        if($status =  $filter_data['status']){
            $letters = $letters->where('status', $status);
        }
        if($supervisor = $filter_data['supervisor']){
            $letters = $letters->where('supervisor', $supervisor);
        }
        if($assign_to = $filter_data['assign_to']){
            $letters = $letters->where('assign_to', $assign_to);
        }

        if($from = $filter_data['from']){
            $letters = $letters->where("DATE_FORMAT(letters.created_at,'%Y-%m-%d') >='$from'");
        }

        if($to = $filter_data['to']){
            $letters = $letters->where("DATE_FORMAT(letters.created_at,'%Y-%m-%d') <='$to'");
        }

        if($filter_data['search'] != ''){
            $match = trim($filter_data['search']);
            $letters = $letters
                ->groupStart()
                    ->like('subject', $match)
                    ->orLike('letter_number',  $match)
                    ->orLike('letters.created_at',  $match)
                    ->orLike('recipient_office',  $match)
                    ->orLike('users.name',  $match)
                ->groupEnd();
        }


        //passing data to view (table display)
        $data = [
            'assignees' => $signers->Assignee(),
            'supervisors' => $signers->Supervisors(),
            'colors' => StatusColors::getColors(),
            'tableDisplays' => $letters->select('user_id,letters.id,
                letters.subject,
                letters.letter_number,
                letters.letter_reference,
                letters.letter_content,
                letters.recipient_office,
                letters.recipient_island,
                letters.created_at,
                letters.supervisor,
                letters.assign_to,
                letters.expires_on,
                letters.status,
                letters.priority,
                users.initials,
            users.name')->join('users','letters.user_id = users.id','full')->orderBy('letters.id','desc')->paginate(10),
            'pager' => $letters->pager
        ];

        //this is for displaying the total counts for to the dashboard table.php
        $new_letters = new Letter();
        $data['statuses']['Draft'] = $new_letters->draftCount();
        $data['statuses']['Pending'] = $new_letters->pendingCount();
        $data['statuses']['Incomplete'] = $new_letters->incompleteCount();
        $data['statuses']['Verified'] = $new_letters->verifyingCount();
        $data['statuses']['Approved'] = $new_letters->approvedCount();
        $data['statuses']['Rejected'] = $new_letters->rejectedCount();
        $data['statuses']['Proceed'] = $new_letters->proceedCount();


        if (session()->get('level_id') == 0){
            return view('admin/dashboard');
        } else {
            return view('letters/index', $data);
        }
    }



	public function create()
    {
        $signers = new User();
        $data['assignees'] = $signers->Assignee();
        $data['supervisors'] = $signers->Supervisors();
        $data['verifiers'] = $signers->Verifiers();
        $data['priority'] = ['Normal','Urgent','Very Urgent','Confidential'];

        return view('letters/create', $data);
    }



	public function store()
    {
        helper(['form', 'url']);
        $letter = new Letter();

        $data = [
            'subject' => $this->request->getVar('subject'),
            'letter_number' => $this->request->getVar('letter_number'),
            'letter_reference' => $this->request->getVar('letter_reference'),
            'recipient_office' => $this->request->getVar('recipient_office'),
            'recipient_island' => $this->request->getVar('recipient_island'),
            'copy' => $this->request->getVar('copy'),
            'expires_on' => $this->request->getVar('expires_on'),
            'supervisor' => $this->request->getVar('supervisor'),
            'verifier' => $this->request->getVar('verifier'),
            'assign_to' => $this->request->getVar('assign_to'),
            'priority' => $this->request->getVar('priority'),
            'letter_content' => $this->request->getVar('letter_content'),
            'user_id' => session()->get('userid'),
            'status' => $this->request->getVar('status')
        ];

        $letter->insert($data);
        return redirect()->to(base_url('letters/'));
//      $letter_id = $letter->getInsertID();
//      return redirect()->to(base_url('letters/'. $letter_id . '/edit'));
    }


    public function edit($id)
    {
        $signers = new User();
        $data['assignees'] = $signers->Assignee();
        $data['supervisors'] = $signers->Supervisors();
        $data['verifiers'] = $signers->Verifiers();
        $data['priority'] = ['Normal','Urgent','Very Urgent','Confidential'];

        $letters = new Letter();
        $data['letter'] = $letters->where('id', $id)->first();
        $data['letterDetailed'] = $letters->letterDetails($id);
        $statusOf = $data['letter']['status'];
        $messages = new Message();
        $data['messages'] = $messages->getLetterMessages($id);
        $data['assignedID'] = session()->get('userid');
        $data['verifierID'] = session()->get('verify_id');

        if($statusOf == 'draft' || $statusOf == 'incomplete' || $statusOf == 'rejected' || $statusOf == 'pending' || $statusOf == 'verified' ||  $statusOf == 'proceed' || session()->get('level_id') == 1 || session()->get('level_id') == 2){
            return view('letters/edit', $data);
        }else{
            return redirect()->to(base_url('403'));
        }
    }



    public function update($id)
    {
        $action = $this->request->getPost('status');

        if ($action == 'draft' || $action == 'pending' ||  $action == 'verified' || $action == 'proceed' ) {
                helper(['form', 'url']);
                $letters = new Letter();
                $letter = $letters->find($id);

                if($letter['user_id'] != session()->get('userid')){
                     if (session()->get('level_id') != 0 && session()->get('level_id') != 1 ){
                        return redirect()->to(base_url('403'));
                    }
                }

                $data = [
                    'subject' => $this->request->getVar('subject'),
                    'letter_number' => $this->request->getVar('letter_number'),
                    'letter_reference' => $this->request->getVar('letter_reference'),
                    'recipient_office' => $this->request->getVar('recipient_office'),
                    'recipient_island' => $this->request->getVar('recipient_island'),
                    'copy' => $this->request->getVar('copy'),
                    'expires_on' => $this->request->getVar('expires_on'),
                    'supervisor' => $this->request->getVar('supervisor'),
                    'verifier' => $this->request->getVar('verifier'),
                    'assign_to' => $this->request->getVar('assign_to'),
                    'priority' => $this->request->getVar('priority'),
                    'letter_content' => $this->request->getVar('letter_content'),
                    'user_id' => session()->get('userid'),
                    'status' => $this->request->getVar('status')
                ];

                $letters->where('id', $id);
                $letters->set($data);
                $letters->update();

                $this->statusUpdate($id,$data['status']);

                return redirect()->to(base_url('letters/'. $letter['id'] . '/edit'));
                // return redirect()->to( base_url('letters') );
        }


        if($action == 'changes'){
                helper(['form', 'url']);
                $letters = new Letter();
                $letter = $letters->find($id);

                // if($letter['supervisor'] != session()->get('userid') || $letter['status'] != 'draft' || $letter['status'] != 'pending' || $letter['status'] != 'verified' ){
                //         return redirect()->to(base_url('403'));
                // }

                $data = [
                    'subject' => $this->request->getVar('subject'),
                    'letter_number' => $this->request->getVar('letter_number'),
                    'letter_reference' => $this->request->getVar('letter_reference'),
                    'recipient_office' => $this->request->getVar('recipient_office'),
                    'recipient_island' => $this->request->getVar('recipient_island'),
                    'copy' => $this->request->getVar('copy'),
                    'letter_content' => $this->request->getVar('letter_content'),
                    'changed_by' => session()->get('userid')
                ];


                $letters->where('id', $id);
                $letters->set($data);
                $letters->update();

                return redirect()->to(base_url('letters/'. $letter['id'] . '/edit'));

                // return redirect()->to( base_url('letters'));
        }


        if($action == 'updatechanges'){
                helper(['form', 'url']);
                $letters = new Letter();
                $letter = $letters->find($id);

                // if($letter['verifier'] != session()->get('userid') && $letter['status'] != 'proceed' && $letter['verifier'] == 38){
                //     // if (session()->get('level_id') != 0 && session()->get('level_id') != 1 ){
                //         return redirect()->to(base_url('403'));
                //     // }
                // }

                $data = [
                    'subject' => $this->request->getVar('subject'),
                    'letter_number' => $this->request->getVar('letter_number'),
                    'letter_reference' => $this->request->getVar('letter_reference'),
                    'recipient_office' => $this->request->getVar('recipient_office'),
                    'recipient_island' => $this->request->getVar('recipient_island'),
                    'copy' => $this->request->getVar('copy'),
                    'letter_content' => $this->request->getVar('letter_content'),
                    'updated_by' => session()->get('userid')
                ];


                $letters->where('id', $id);
                $letters->set($data);
                $letters->update();

                // return redirect()->to(base_url('letters/'. $letter['id'] . '/edit'));

                return redirect()->to( base_url('letters'));
        }
    }




    public function view($id)
    {
        helper(['form', 'url']);

        $letter = new Letter();
        $data['letter'] = $letter->letterDetails($id);
        return view('letters/view', $data);
    }




    public function delete($id)
    {
        $deleteLetter = new Letter();
        $delLetter = $deleteLetter->find($id);
        $statusOf = $delLetter['status'];

        // check if logged in login is matched to login who written the letter and the level of the login
        if($delLetter['user_id'] != session()->get('userid')){
            if (session()->get('level_id') != 0 && session()->get('level_id') != 1 ) {
                    return redirect()->to(base_url('403'));
                }
        }

        if ($statusOf == 'draft' || $statusOf == 'incomplete' || $statusOf == 'rejected') {
            $deleteLetter->delete($id);
            return $this->response->redirect(site_url('letters'));
        } else {
            return redirect()->to(base_url('403'));
        }

    }




    public function statusUpdate($id,$status = NULL)
    {
        helper(['form', 'url']);

        $SmS = new \App\Libraries\Sms();
        $letter = new Letter();

        //get user object
        $user_id = session()->get('userid');
        $user = new User();
        $user->find($user_id);

        $current_letter = $letter->find($id);
        $sms_status = $letter->letterDetails($id);
        $data = ['status' => $status ?? $this->request->getVar('status')];

        //calling function from User model
        if (!$user->canUpdateStatus($data['status'])){
            unset($data['status']);
        }

//       switch ($user_level) {
//           case 0:
//                if (! in_array($data['status'], ['draft', 'pending', 'verified', 'incomplete', 'approved', 'rejected'])) {
//                    unset($data['status']);
//                }
//                break;
//           case 1:
//                if (! in_array($data['status'], ['approved', 'rejected'])) {
//                    unset($data['status']);
//                }
//                break;
//           case 2:
//                if (! in_array($data['status'], ['verified', 'incomplete'])) {
//                    unset($data['status']);
//                }
//                break;
//           case 3:
//                if (! in_array($data['status'], ['draft', 'pending'])) {
//                    unset($data['status']);
//                }
//                break;
//        }

        $letter->where('id', $id);
        $letter->set('status', $data['status'] ?? $current_letter['status']);
        $letter->update();
        $letter = new Letter();
        $current_letter = $letter->find($id);

        if ($current_letter['status'] == "pending"){
            $SmS->send($sms_status['supervisorContact'],'You have a new Letter to Verify. REF#:'.$current_letter['letter_number']);
        }
        if ($current_letter['status'] == "verified"){
            $SmS->send($sms_status['verifierContact'],'You have a new Letter to Proceed. REF#:'.$current_letter['letter_number']);
        } 
        if ($current_letter['status'] == "verified" && $current_letter['verifier'] == 38){
            $SmS->send($sms_status['assignedContact'],'You have a new Letter to Approve. REF#:'.$current_letter['letter_number']);
        }
        if ($current_letter['status'] == "proceed"){
            $SmS->send($sms_status['assignedContact'],'You have a new Letter to Approve. REF#:'.$current_letter['letter_number']);
        }
        if ($current_letter['status'] == "incomplete"){
            $SmS->send($sms_status['contact'],'Letter is Incomplete. REF#:'.$current_letter['letter_number']);
        }
        if ($current_letter['status'] == "approved"){
            $SmS->send($sms_status['contact'],'Letter is Approved. REF#:'.$current_letter['letter_number']);
        }
        if ($current_letter['status'] == "rejected"){
            $SmS->send($sms_status['contact'],'Letter is Rejected. REF#:'.$current_letter['letter_number']);
        }

        return redirect()->to(base_url('letters'));
    }




    public function letterView($id)
    {
        helper(['form', 'url']);
        $letter = new Letter();

        $uCal = new \App\Libraries\uCal();
        $uCal->setLang("dv");

        $data['letter'] = $letter->letterDetails($id);
        return view('letters/letterTemplate', $data);
    }



    public function letterViewInd($id)
    {
        helper(['form', 'url']);
        $letter = new Letter();

        $uCal = new \App\Libraries\uCal();
        $uCal->setLang("dv");

        $data['letter'] = $letter->letterDetails($id);
        return view('letters/letterTemplateIndividual', $data);
    }



}