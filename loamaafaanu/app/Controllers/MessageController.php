<?php
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Letter;
use App\Models\Message;


class MessageController extends BaseController
{
    public function store($id)
    {
        helper(['form', 'url']);
        $letter = new Letter();
        $message = new Message();

        $data = [
            'letter_id' => $id,
            'user_id' => session()->get('userid'),
            'message' => $this->request->getVar('message')
        ];

        $message->insert($data);

        return redirect()->to(base_url('letters/'. $id . '/edit'));

    }



}