<?php

namespace App\Models;

use CodeIgniter\Model;


class Letter extends Model
{
    protected $table = 'letters';
    protected $primaryKey = 'id';
    protected $returnType = 'array';
    protected $allowedFields = ['subject','letter_number','letter_reference','expires_on','supervisor','verifier','assign_to','priority','letter_content','recipient_office','recipient_island','copy','status','user_id','changed_by','updated_by'];
//  protected $useSoftDeletes = true;
//  protected $useTimestamps = false;
//  protected $createdField  = 'created_at';
//  protected $updatedField  = 'updated_at';
//  protected $deletedField  = 'deleted_at';
//  this functions will count all the status for the statuscards



    public function draftCount()
    {
        return  $this->select('status')->where('status','Draft')->countAllResults();
    }

    public function pendingCount()
    {
        return  $this->select('status')->where('status','Pending')->countAllResults();
    }

    public function incompleteCount()
    {
        return  $this->select('status')->where('status','Incomplete')->countAllResults();
    }

    public function approvedCount()
    {
        return  $this->select('status')->where('status','Approved')->countAllResults();
    }

    public function rejectedCount()
    {
        return  $this->select('status')->where('status','Rejected')->countAllResults();
    }

    public function verifyingCount()
    {
        return  $this->select('status')->where('status','Verified')->countAllResults();
    }

     public function proceedCount()
    {
        return  $this->select('status')->where('status','Proceed')->countAllResults();
    }
    
    public function letterDetails($id)
    {
        $this->select('user_id,letters.id,
        letters.subject,
        letters.letter_number,
        letters.letter_reference,
        letters.letter_content,
        letters.recipient_office,
        letters.recipient_island,
        letters.created_at,
        letters.supervisor,
        letters.supervisor,
        letters.assign_to,
        letters.expires_on,
        letters.status,
        letters.priority,
        letters.copy,
        letters.verifier,
        letters.changed_by,
        letters.updated_by,
        users.name,
        users.contact,
        users.initials,
        assigned.contact AS assignedContact,
        supusers.contact AS supervisorContact,
        supusers.id AS supervisorID,
        verifiers.contact AS verifierContact,
        verifiers.id AS verifierID,
        verifiers.name AS verifierName,
        assigned.sex AS gender,
        assigned.designation_mv As dhiDesig,
        assigned.sign AS signature,
        assigned.id AS assignedID,
        supusers.name AS supervisorName,
        assigned.name_mv AS dhiName,
        assigned.name AS assignedName
        ');
        $this->join('users AS supusers','letters.supervisor = supusers.id','full');
        $this->join('users AS verifiers','letters.verifier = verifiers.id','left');
        $this->join('users AS assigned','letters.assign_to = assigned.id','full');
        $this->join('users','letters.user_id = users.id','full');
        return $this->find($id);
    }

}