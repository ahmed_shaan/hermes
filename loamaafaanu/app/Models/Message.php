<?php

namespace App\Models;

use CodeIgniter\Model;

class Message extends Model
{
    protected $table = 'messages';
    protected $primaryKey = 'id';
    protected $returnType = 'array';
    protected $allowedFields = ['user_id','letter_id','message'];
//  protected $useSoftDeletes = true;
//  protected $useTimestamps = false;
//  protected $createdField  = 'created_at';
//  protected $updatedField  = 'updated_at';
//  protected $deletedField  = 'deleted_at';


    //get messages according to the letter id
    public function getLetterMessages($letter_id)
    {
        $this->where('letter_id',$letter_id);
        $this->join('users','messages.user_id = users.id','full');
        $this->join('letters','letters.id = messages.letter_id','full');
        $this->orderBy('messages.msg_created_at','DESC');
        return $this->findAll();
    }

}