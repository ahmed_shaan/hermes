<?php

namespace App\Models;

use CodeIgniter\Model;


class User extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $permissions = [
        0 => ['draft', 'pending', 'verified', 'proceed','incomplete', 'approved', 'rejected'],
        1 => ['approved', 'rejected', 'proceed','incomplete'],
        2 => ['verified', 'incomplete','proceed'],
        3 => ['draft', 'pending']
    ];

    public function Assignee()
    {
        return $this->select('id,name')->where('level_id','1')->findAll();
    }


    public function Supervisors()
    {
        return $this->select('id,name')->where('level_id','2')->findAll();
    }

    public function Verifiers()
    {
        return $this->select('id,name')->where('verify_id','1')->findAll();
    }


    public function userProfile($id)
    {
        return $this->select('id,name,sex,level_id,rank,designation,division,nid,password,contact,pp')->find($id);
    }


    public function canUpdateStatus($status)
    {
        $user_level = session()->get('level_id');
        return in_array($status,$this->permissions[$user_level]);
    }


    public function updateAvatar($path, $id )
    {
        $builder = $this->db->table('users');
        $builder->where('id',$id);
        $builder->update(['pp'=>$path]);
        if($this->db->affectedRows() > 0){
            return true;
        } else {
            return false;
        }

    }

    public function remAvatar($id)
    {
        $builder = $this->db->table('users');
        $builder->where('id',$id);
        $builder->update(['pp'=>'']);
        if($this->db->affectedRows() > 0){
            return true;
        } else {
            return false;
        }

    }


    public function updatePassword($npwd, $id)
    {
        $builder = $this->db->table('users');
        $builder->where('id',$id);
        $builder->update(['password'=>$npwd]);
        if($this->db->affectedRows() > 0){
            return true;
        } else {
            return false;
        }
    }




}