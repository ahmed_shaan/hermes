<div class="hidden mx-20 mt-20 md:gap-2 md:mt-20 md:grid md:grid-cols-4 lg:mt-20 lg:mx-10 lg:grid lg:grid-cols-7 lg:gap-4"><!-- status card -->
    <?php foreach ($statuses as $key=>$status) :?>
        <div class="<?= $colors[strtolower($key)]?> text-white rounded-md flex justify-between items-center px-5 py-4 shadow-md hover:shadow-lg">
            <p class="capitalize"><?= $key ?></p>
            <p class="text-lg"><?= $status ?></p>
        </div>
    <?php endforeach; ?>
</div>