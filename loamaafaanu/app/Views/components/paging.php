<?php
    $pager->setSurroundCount(2);
?>

<nav aria-label="<?= lang('Pager.pageNavigation') ?>" class="mt-5">
    <ul class="pagination flex flex-row">
        <?php if ($pager->hasPrevious()) : ?>
            <li>
                <a class="btn bg-gray-700 px-4 py-1 text-xs border border-gray-600 text-gray-100 hover:bg-gray-600" href="<?= $pager->getFirst() ?>" aria-label="<?= lang('Pager.first') ?>">
                    <span aria-hidden="true"><?= lang('Pager.first') ?></span>
                </a>
            </li>&nbsp;
            <li>
                <a class="btn bg-gray-700 px-4 py-1 text-xs border border-gray-600 text-gray-100 hover:bg-gray-600" href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
                    <span aria-hidden="true"><?= lang('Pager.previous') ?></span>
                </a>
            </li>&nbsp;
        <?php endif ?>

        <?php foreach ($pager->links() as $link) : ?>
            <li <?= $link['active'] ? 'class="active"' : '' ?>>
                <a href="<?= $link['uri'] ?>" class="btn bg-gray-700 px-4 py-1 text-xs border border-gray-600 text-gray-100 hover:bg-gray-600">
                    <?= $link['title'] ?>
                </a>
            </li>&nbsp;
        <?php endforeach ?>

        <?php if ($pager->hasNext()) : ?>
            <li>
                <a class="btn bg-gray-700 px-4 py-1 text-xs border border-gray-600 text-gray-100 hover:bg-gray-600" href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
                    <span aria-hidden="true"><?= lang('Pager.next') ?></span>
                </a>
            </li>&nbsp;
            <li>
                <a class="btn bg-gray-700 px-4 py-1 text-xs border border-gray-600 text-gray-100 hover:bg-gray-600" href="<?= $pager->getLast() ?>" aria-label="<?= lang('Pager.last') ?>">
                    <span aria-hidden="true"><?= lang('Pager.last') ?></span>
                </a>
            </li>&nbsp;
        <?php endif ?>
    </ul>
</nav>