<div class="mt-20 mx-10 flex justify-between items-center"><!--top buttons-->
    <div class="flex justify-between items-center" >
        <a href="<?= base_url('/letters') ?>" class="text-gray-600 text-xs hover:text-gray-700 font-medium rounded inline-flex items-center">
            <svg class="fill-current w-8 h-8" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.707-10.293a1 1 0 00-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L9.414 11H13a1 1 0 100-2H9.414l1.293-1.293z" clip-rule="evenodd"></path></svg>
        </a>
<!--        <span class="bg-orange-500 ml-1 text-xs rounded py-1 px-10 text-white">--><?//= $colors[strtolower($tableDisplay['status'])]; ?><!--</span>-->
    </div>

<?php if(isset($letter)): ?>

    <form action="<?= base_url('/letters/' . $letter['id'] . '/status-update'); ?>" method="post">
    <div class="">
        <?php if(session()->get('level_id') == 1 && session()->get('userid') == $letterDetailed['assignedID']): ?>
            <?php if($letter['status'] == 'approved' || $letter['status'] == 'proceed'): ?>
                <button type="submit" name="status" value="rejected" class="bg-red-500 hover:bg-red-600 text-white text-xs hover:text-white font-medium py-2 px-5 rounded inline-flex items-center">Reject</button>
                <button type="submit" name="status" value="approved" class="bg-green-500 hover:bg-green-600 text-white text-xs hover:text-white font-medium py-2 px-5 rounded inline-flex items-center">Approve</button>
            <?php endif; ?>
        <?php endif; ?>



        <?php if(session()->get('level_id') == 1 && session()->get('userid') == $letterDetailed['verifierID']) : ?>
            <?php if($letter['status'] == 'verified'): ?>
                <button type="submit" name="status" value="proceed" class="bg-blue-800 hover:bg-blue-600 text-white text-xs hover:text-white font-medium py-2 px-5 rounded inline-flex items-center">Proceed</button>
            <?php endif; ?>
        <?php endif; ?>



        <?php $supervisor_id = isset($letterDetailed['supervisorID']) ? $letterDetailed['supervisorID'] : ''; ?>

        <?php if(session()->get('level_id') == 1 || session()->get('level_id') == 2 || session()->get('userid') == $supervisor_id): ?>
            <?php if(($letter['status'] == 'pending') || ($letter['status'] == 'verified') ||  ($letter['status'] == 'proceed') || ($letter['verifier'] == 38 )): ?>
                    <button type="submit" name="status" value="incomplete" class="bg-orange-500 hover:bg-orange-600 text-white text-xs hover:text-white font-medium py-2 px-5 rounded inline-flex items-center">Incomplete</button>
            <?php endif; ?>
        <?php endif; ?>



        <?php if(session()->get('level_id') == 2 && session()->get('userid') == $supervisor_id): ?>

        <?php if(($letter['status'] == 'pending' || $letter['status'] == 'verified') && $letter['verifier'] != 38): ?>
            <button type="submit" name="status" value="verified" class="bg-teal-500 hover:bg-teal-600 text-white text-xs hover:text-white font-medium py-2 px-5 rounded inline-flex items-center">Verify</button>
        <?php endif; ?>

        <?php if($letter['verifier'] == 38): ?>
              <button type="submit" name="status" value="proceed" class="bg-blue-800 hover:bg-blue-600 text-white text-xs hover:text-white font-medium py-2 px-5 rounded inline-flex items-center">Proceed</button>
            <?php endif; ?>
        <?php endif; ?>








    </form>

    <?php endif; ?>
    </div>

</div>

<hr class="mt-8">