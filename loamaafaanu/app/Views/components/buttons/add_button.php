    <div class="mt-8 w-full">
        <form class="" action="<?= base_url('letters') ?>" method="get">
            <div class="my-20 mx-20 md:mx-10 md:my-10 lg:grid lg:grid-cols-4 lg:gap-2 lg:mx-10 lg:flex lg:items-center lg:align-middle">
                    <div class="">
                        <select name="status" class="w-full py-2 px-2 rounded border text-gray-600 text-xs">
                            <option selected value="">Status</option>
                            <?php foreach ( $statuses as $key => $status  ) :?>
                                <option value="<?= $key ?>" <?php if( isset($_GET['status']) && $_GET['status'] == $key) echo 'selected';  ?>><?= $key ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="">
                        <select name="supervisor" class="w-full py-2 px-2 border rounded text-gray-600 text-xs">
                            <option selected value="">Supervisor</option>
                            <?php foreach ( $supervisors as $supervisor  ) :?>
                                <option value="<?= $supervisor['id'] ?>" <?php if( isset($_GET['supervisor']) && $_GET['supervisor'] == $supervisor['id']) echo 'selected';  ?>><?= $supervisor['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <div class="">
                    <select name="assign_to" class="w-full py-2 px-2 border rounded text-gray-600 text-xs">
                        <option selected value="">Assigned To</option>
                        <?php foreach ( $assignees as $assignee ) :?>
                            <option value="<?= $assignee['id'] ?>" <?php if( isset($_GET['assign_to']) && $_GET['assign_to'] == $assignee['id']) echo 'selected';  ?>><?= $assignee['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="">
                    <input name="search" class="w-full py-2 px-2 border rounded text-gray-600 text-xs" placeholder="Search here" />
                </div>

                <div class="">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="from">From</label>
                    <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="date" name="from" id="from">
                </div>
                <div class="">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="to">To</label>
                    <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="date" name="to" id="to">
                </div>

                <div class="mt-8">
                    <input class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white text-xm py-2 px-6 border rounded" type="submit" value="Search">
                    <a href="<?= base_url('/letters')?>" class="btn bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white  py-2 px-6 border rounded"><span>All</span></a>
                </div>
        </form>
            <!-- Add button will be visible based on user level -->
            <?php if(session()->get('level_id') == 3 || session()->get('level_id') == 2 ): ?>
                <div class="mt-8">
                    <a href="<?= base_url('/letters/create') ?>" class="float-right bg-teal-600 lg:hover:bg-teal-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center">
                        <svg class="fill-current w-4 h-4 mr-2" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z" clip-rule="evenodd"></path></svg><span>Add New</span>
                    </a>
                </div>
            <?php endif; ?>
</div>

