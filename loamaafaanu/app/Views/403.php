<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
</head>
<body class="bg-red-500 font-body antialiased">
<div class="flex h-screen">
    <div class="m-auto">
        <div class="mt-2 text-center">
            <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
            <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_slGFhN.json"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop  autoplay></lottie-player>
            <h2 class="text-m text-white mb-10">Your not authorized to make any changes</h2>
            <a class="bg-gray-100 hover:bg-gray-300 text-red-500 font-bold py-2 px-4 rounded" href="<?php echo base_url('letters'); ?>">Back</a>
        </div>
    </div>
</div>
</body>
</html>