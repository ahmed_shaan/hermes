<?= $this->extend('/layouts/main') ?>

<?= $this->section('content') ?>

 <div class="mt-20 mx-10 content-center">
     <div class="flex justify-center">
      <div>
          <a href="<?= base_url('/profile/'. $usersProfile['id'] . '/remove');?>">x</a>
          <?php if($usersProfile['pp'] != ''): ?>
                <img src="<?= $usersProfile['pp']; ?>" class="relative w-32 h-32 rounded-full object-cover">
           <?php else: ?>
                <?php if (session()->get('sex') == "Male"): ?>
                    <img src="<?= base_url() ?>/assets/profiles/male.png" alt="Male Avatar" class="relative w-32 h-32 rounded-full object-cover">
                <?php endif; ?>
                    <?php if (session()->get('sex') == "Female"): ?>
                  <img src="<?= base_url() ?>/assets/profiles/female.jpg" alt="Female Avatar" class="relative w-32 h-32 rounded-full object-cover">
                <?php endif; ?>
            <?php endif; ?>
        </div>
     </div>


     <div class="px-5 py-5 text-center">
         <h1 class="pl-5 text-gray-700 "><?= $usersProfile['name'] ?></h1>
         <h2 class="pl-5 text-gray-600"><?= $usersProfile['designation'] ?></h2>
         <h3 class="pl-5 text-gray-500"><?= $usersProfile['nid'] ?> | <?= $usersProfile['rank'] ?> | <?= $usersProfile['sex'] ?> | <?= $usersProfile['division'] ?></h3>
         <form action="<?= base_url('passChange');?>" method="post">
         <div class="mb-1 w-30 pt-6">
                 <label class="block text-gray-600 text-sm font-semibold mb-2" for="password">
                     Change Password
                 </label>
                 <input class="shadow text-xs appearance-none border border-teal-500 rounded w-40 py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password"name="opwd" type="password" placeholder="Current Password">
                 <input class="shadow text-xs appearance-none border border-teal-500 rounded w-40 py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" name="npwd" type="password" placeholder="New Password">
                 <input class="shadow text-xs appearance-none border border-teal-500 rounded w-40 py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" name="cnpwd" type="password" placeholder="Confirm Password">
                 <button class="bg-gray-700 hover:bg-gray-600 text-xs text-white py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                     Change
                 </button>
             <?= form_close(); ?>

             <form action="<?= base_url('avatar');?>" enctype="multipart/form-data" method="post">
             <input class="bg-gray-700 hover:bg-gray-600 text-xs text-white py-2 px-4 rounded focus:outline-none focus:shadow-outline" name="avatar" value="Upload" type="file">
             <button class="bg-gray-700 hover:bg-gray-600 text-xs text-white py-2 px-4 rounded focus:outline-none focus:shadow-outline"  value="Save" type="submit">
                 Upload Photo
             </button>
             <?= form_close(); ?>
         </div>

         <?php if(isset($validation)): ?>
             <div  id="SuccessAvatar"  role="alert">
                 <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                     Error
                 </div>
                 <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                     <p><?= $validation->listErrors(); ?></p>
                 </div>
             </div>
         <?php endif; ?>

         <?php if(session()->getTempdata('success')): ?>
             <div id="SuccessAvatar" class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
                 <div class="flex">
                     <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                     <div>
                         <p class="font-bold">Successful</p>
                         <p class="text-sm"><?= session()->getTempdata('success'); ?></p>
                     </div>
                 </div>
             </div>
         <?php endif; ?>

         <?php if(session()->getTempdata('error')): ?>
             <div  id="SuccessAvatar" role="alert">
                 <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                     Error
                 </div>
                 <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                     <p><?= session()->getTempdata('error'); ?></p>
                 </div>
             </div>
         <?php endif; ?>
     </div>
        <script>
            setTimeout(function(){
                document.getElementById('SuccessAvatar').style.display = 'none';
            }, 3000); // 10000ms = 10s
        </script>
 </div>

<?= $this->endSection() ?>