<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
        <title>Loamaafaanu</title>
    </head>
    <body class="bg-gray-300 mb-10 font-body">
    <?php if(session()->get('isLoggedIn')): ?>
        <div class=""><!-- content wrapper -->
            <div class="fixed left-0 right-0 top-0 flex justify-between items-center bg-white shadow-sm z-40 do-not-print"><!-- navbar -->
                <a class="hidden md:block md:text-gray-800 md:font-body md:text-medium md:mx-10 md:my-2 md:uppercase" href="<?= base_url('/letters')?>">Loamaafaanu : <span class="text-gray-600 text-sm lowercase">Letter Management</span></a>
                <span class="bg-white-500 text-sm font-medium py-2 px-4 border-blue-700 rounded mx-10 my-2 text-gray-700">Logged in as <span class="font-bold text-gray-800"><?= session()->get('username') ?></span><a href="<?= base_url('profile/' . session()->get('userid'))  ?>"><span class=" hover:bg-gray-300 text-gray-800 ml-2 px-2 py-2 rounded border text-xs border-gray-400 rounded">Profile</span></a>
                    <a href="<?= base_url('/logout')?>"><span class="hover:bg-gray-300 text-gray-800 text-xs ml-1 px-2 py-2 rounded border border-gray-400 rounded">Logout</span></a></span>
            </div>
            <?php endif; ?>
            <?= $this->renderSection('content') ?>
        </div>
        <script src="<?php base_url()?>/assets/js/jquery-3.5.1.min.js"></script>
        <script src="<?php base_url()?>/assets/js/ckeditor/ckeditor.js"></script>
        <script>
        CKEDITOR.replace( 'letter_content' );
    </script>
    </body>
</html>