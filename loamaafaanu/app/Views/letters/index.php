<?= $this->extend('/layouts/main') ?>

<?= $this->section('content') ?>
        <?= $this->include('/components/status_cards') ?>

        <?= $this->include('/components/buttons/add_button') ?>
        <?= $this->include('/letters/_table') ?>

<?= $this->endSection() ?>