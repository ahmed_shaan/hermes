<?= $this->extend('/layouts/main') ?>

<?= $this->section('content') ?>
<?= $this->include('components/buttons/status_buttons') ?>

<div class="mt-6 mx-20"> <!--Letter-->
    <h1 class="text-gray-600 text-lg">Edit #<?= $letter['id'] ?></h1>
    <div class="mt-2 w-full"><!-- Letter Form -->
        <form action="<?= base_url('/letters/' . $letter['id'] . '/update');?>" method="post" enctype="multipart/form-data">
            <?= $this->include('letters/_form'); ?>
        </form>
    </div>
    <?= $this->include('letters/messages') ?>
</div>

<?= $this->endSection('content') ?>