<?php //if($tableDisplays): ?>
<!--    --><?php //foreach ($tableDisplays as $tableDisplay): ?>
<!--        <tr>-->
<!--            <td class="text-xs md:border md:text-sm md:text-center px-2">--><?//= $tableDisplay['id']; ?><!--</td>-->
<!--            <td class="text-xs md:border md:text-sm md:pl-1 md:max-w-sm">--><?//= $tableDisplay['subject']; ?><!--</td>-->
<!--            <td class="text-xs md:border md:text-sm md:text-center  md:max-w-sm">--><?//= $tableDisplay['name']; ?><!--</td>-->
<!--            <td class="text-xs md:border md:text-sm md:text-center">--><?//= $tableDisplay['created_at']; ?><!--</td>-->
<!--            <td class="text-xs md:border md:text-sm md:text-center">--><?//= $tableDisplay['expires_on']; ?><!--</td>-->
<!--            <td class="text-xs md:border md:text-sm md:text-center">--><?//= $tableDisplay['priority']; ?><!--</td>-->
<!--            <td class="text-xs md:border md:text-sm md:text-center"><span class="--><?//= $colors[strtolower($tableDisplay['status'])];?><!-- text-xs md:px-2 md:py-1 rounded text-white">-->
<!--                    --><?//= ucwords($tableDisplay['status']); ?>
<!--                </span></td>-->
<!--            <td class="border ">-->
<!--                <div class="flex justify-start">-->
<!--                    <a href="--><?//= base_url('lettersTemplate/' . $tableDisplay['id'])  ?><!--" target="_blank" class="hidden text-gray-600 font-xs md:py-2 px-2 md:inline-flex md:items-center md:hover:text-gray-700">-->
<!--                        <svg class="w-5 h-5 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>-->
<!--                        <span class="text-sm">Download</span>-->
<!--                    </a>-->
<!--                    <a href="--><?//= base_url('letters/' . $tableDisplay['id'])  ?><!--" class="hidden text-gray-600 font-xs md:py-2 md:px-2 md:inline-flex md:items-center hover:text-green-500">-->
<!--                        <svg class="fill-current w-4 h-4 mr-1" fill="currentColor" viewBox="0 0 20 20"><path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path><path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd"></path></svg>-->
<!--                        <span class="text-sm">View</span>-->
<!--                    </a>-->
<!--                    <a href="--><?//= base_url('letters/' . $tableDisplay['id'] . '/edit') ?><!--" class="text-xs text-gray-600 md:font-xs  md:py-2 md:px-2 md:mr-1 md:inline-flex md:items-center hover:text-blue-500">-->
<!--                        <svg class="fill-current w-4 h-4 mr-1" fill="currentColor" viewBox="0 0 20 20"><path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"></path><path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd"></path></svg>-->
<!--                        <span class="text-sm">Edit</span>-->
<!--                    </a>-->
<!--                    <a href="--><?//= base_url('letters/' . $tableDisplay['id'] . '/delete') ?><!--" class="hidden text-gray-600 font-xs md:py-2 md:px-2  md:mr-1 md:inline-flex md:items-center hover:text-red-500">-->
<!--                        <svg class="fill-current w-4 h-4 mr-1" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg>-->
<!--                        <span class="text-sm">Delete</span>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </td>-->
<!--        </tr>-->
<!--    --><?php //endforeach; ?>
<?php //endif; ?>

<?php if($tableDisplays): ?>
    <?php foreach ($tableDisplays as $tableDisplay): ?>
        <tr class="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap lg:mb-0">
            <td class="w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-center  md:border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden px-2 absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase">#</span>
                <?= $tableDisplay['id']; ?>
            </td>
            <td class="w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-left md:pl-2 md:border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden px-2 absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase">Subject</span>
                <?= $tableDisplay['subject']; ?>
            </td>
            <td class="w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-center  md:border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden px-2 absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase">Created By</span>
                <?= $tableDisplay['name']; ?>
            </td>
            <td class="w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-center  md:border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden px-2 absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase">Created At</span>
                <?= $tableDisplay['created_at']; ?>
            </td>
            <td class="w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-center  md:border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden px-2 absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase">Letter Number</span>
                <?= $tableDisplay['letter_number']; ?>
            </td>
            <td class="dhivehibas w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-right md:pr-2  md:border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden px-2 absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase dhivehibas">Recipient Office</span>
                <?= $tableDisplay['recipient_office']; ?>
            </td>
            <td class="w-full lg:w-auto text-xs md:border-none md:text-sm text-gray-800 text-center md:text-center  md:border-b block lg:table-cell relative lg:static">
                <span class="<?= $colors[strtolower($tableDisplay['status'])];?> text-xs px-1 py-1 md:px-1 md:py-1 rounded text-white"><?= ucwords($tableDisplay['status']); ?></span>
                <span class="lg:hidden absolute top-0 text-grey-700 left-0 px-2 py-1 text-xs font-bold uppercase">Status</span>
            </td>
            <td class="w-full lg:w-auto p-3 text-gray-800 text-center md:border-none md:border-b text-center block lg:table-cell relative lg:static">
                <span class="lg:hidden absolute top-0 left-0 text-grey-700 px-2 py-1 text-xs font-bold uppercase">Actions</span>
                <div class="flex justify-right md:justify-evenly">
                    <a href="<?= base_url('lettersTemplate/' . $tableDisplay['id'])  ?>" target="_blank" class="hidden text-gray-600 font-xs md:py-2 px-2 md:inline-flex md:items-center md:hover:text-gray-700">
                        <svg class="w-5 h-5 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>
                        <span class="text-sm">Download</span>
                    </a>
                    <!-- <a href="<?= base_url('lettersTemplateIndividual/' . $tableDisplay['id'])  ?>" target="_blank" class="hidden text-gray-600 font-xs md:py-2 px-2 md:inline-flex md:items-center md:hover:text-gray-700">
                        <svg class="w-5 h-5 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>
                        <span class="text-sm">Individual</span>
                    </a> -->
                    <a href="<?= base_url('letters/' . $tableDisplay['id'])  ?>" class="hidden text-gray-600 font-xs md:py-2 md:px-2 md:inline-flex md:items-center hover:text-green-500">
                        <svg class="fill-current w-4 h-4 mr-1" fill="currentColor" viewBox="0 0 20 20"><path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path><path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd"></path></svg>
                        <span class="text-sm">View</span>
                    </a>
                    <a href="<?= base_url('letters/' . $tableDisplay['id'] . '/edit') ?>" class="text-xs text-gray-600 mt-3 md:mt-0 md:font-xs  md:py-2 md:px-2 md:mr-1 md:inline-flex md:items-center hover:text-blue-500">
                        <svg class="fill-current w-4 h-4 mr-1" fill="currentColor" viewBox="0 0 20 20"><path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"></path><path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd"></path></svg>
                        <span class="text-sm">Edit</span>
                    </a>
                    <a href="<?= base_url('letters/' . $tableDisplay['id'] . '/delete') ?>" class="hidden text-gray-600 font-xs md:py-2 md:px-2  md:mr-1 md:inline-flex md:items-center hover:text-red-500" onclick="return confirm('Are you sure want to Delete this Letter?')">
                        <svg class="fill-current w-4 h-4 mr-1" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd"></path></svg>
                        <span class="text-sm">Delete</span>
                    </a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
<?php endif; ?>