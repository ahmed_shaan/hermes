<div class="overflow-x-auto">
<!--<table id="myTable" class="text-sm w-full text-left bg-white">-->
<!--    <thead class=" bg-gray-700">-->
<!--        <tr class="text-xs md:text-sm md:text-semibold bg-red font-light text-white">-->
<!--            <th class="md:border md:text-center py-1">#</th>-->
<!--            <th class="md:border md:text-center">Subject</th>-->
<!--            <th class="md:border md:text-center">Created By</th>-->
<!--            <th class="md:border md:text-center">Created Date</th>-->
<!--           <th class="border text-center">Supervisor</th>-->
<!--          <th class="border text-center">Assigned To</th>-->
<!--            <th class="md:border md:text-center">Expires On</th>-->
<!--            <th class="md:border md:text-center">Priority</th>-->
<!--            <th class="md:border md:text-center">Status</th>-->
<!--            <th class="md:border md:text-center">Actions</th>-->
<!--        </tr>-->
<!--    </thead>-->
<!--    <tbody class="text-gray-700 stripped">-->

    <table class=" text-sm w-full text-left bg-white border-none">
        <thead class="bg-gray-700">
        <tr class="text-xs md:text-sm md:text-semibold bg-red font-light text-white">
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">#</th>
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Subject</th>
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Created By</th>
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Created Date</th>
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Letter Number</th>
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Recipient Office</th>
<!--        <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Priority</th>-->
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Status</th>
            <th class="p-3 font-bold md:border md:text-center py-1 border-none hidden lg:table-cell">Actions</th>
        </tr>
        </thead>
        <tbody class="text-gray-700 stripped border-none px-2">