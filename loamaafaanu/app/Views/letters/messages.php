<!-- Messages UI-->
<div class="mt-5 pt-4 bg-white rounded">
    <div class="pl-4">
        <h1 class="text-green-500">Messages</h1>
    </div>
    <div class="px-4 py-4 overflow-scroll bg-gray-300 mx-4 my-4">
        <div class="w-full h-56">
<!--                        --><?php //echo var_dump($messages); dd();?>
            <?php foreach ($messages as $message) : ?>
                <div class="break-all rounded mx-5 my-5 px-3 py-3 bg-white text-sm">
                        <p class="px-3 py-1 rounded text-white float-left bg-green-600 text-xs"><?= $message['name'] ?> - <span><?= $message['msg_created_at'] ?></span></p>
                    <p class="pl-3 py-3 whitespace-normal clear-both"><?= $message['message'] ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <form action="<?= base_url('/messages/' . $letter['id'] . '/store');?>" method="post">
        <div class="px-4 py-4">
            <input type="text" class="w-full py-2 px-2 bg-gray-300 text-sm" id="" name="message">
        </div>
        <div class="flex justify-end items-end pr-4 pb-5">
            <button class="bg-green-600 text-white text-xs hover:text-white py-2 px-5 rounded hover:bg-green-700" type="submit">Send</button>
        </div>
    </form>
</div>