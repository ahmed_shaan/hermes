<div class="md:grid md:grid-cols-3 md:gap-3">
    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="subject">Subject</label>
        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="text" name="subject" id="subject" value="<?php echo $letter['subject'] ?? null ?>" required>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="letterNumber">Letter Number</label>
        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="text" name="letter_number" id="letter_number" value="<?php echo $letter['letter_number'] ?? null ?>" required>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="letterRefNumber">Letter Reference Number</label>
        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="text" name="letter_reference" id="reference_number" value="<?php echo $letter['letter_reference'] ?? null ?>" required>
    </div>

<!--    <div class="">-->
<!--        <label class="block text-gray-700 text-sm font-bold mb-2" for="letterRefNumber">Reference Letter</label>-->
<!--        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="file" name="letter_reference" id="reference_number" value="--><?php //echo $letter['letter_reference'] ?? null ?><!--" required>-->
<!--    </div>-->

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="RecipientOffice">Recipient Office</label>
        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm dhivehibas"  dir="rtl" type="text" name="recipient_office" id="recipient_office" value="<?php echo $letter['recipient_office'] ?? null ?>" required>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="receipientIsland">Recipient Island</label>
        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm dhivehibas"  dir="rtl" type="text" name="recipient_island" id="recipient_island" value="<?php echo $letter['recipient_island'] ?? null ?>" required>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="expireDate">Expires On</label>
        <input class="w-full py-2 px-2 rounded text-gray-600 text-sm" type="date" name="expires_on" id="expireDate" value="<?php echo $letter['expires_on'] ?? null ?>" required>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="VerifiedBy">Supervisor</label>
        <select id="supervisor" name="supervisor" class="w-full py-2 px-2 rounded text-gray-600 text-sm" onchange="supervise(),approver()" required>
        <!-- <option value="">Please Select</option> -->
            <?php foreach ( $supervisors as $supervisor  ) :?>
                <option value="<?= $supervisor['id']?>"<?php
                    if(isset($letter)){
                        echo ($supervisor['id'] == $letter['supervisor']) ? 'selected' : '';
                    }
                    ?>><?= $supervisor['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="VerifiedBy">Verifiers</label>
        <select id="verifier" name="verifier" class="w-full py-2 px-2 rounded text-gray-600 text-sm" onchange="verfier(),approver()" required>
        <!-- <option value="">Please Select</option> -->
            <?php foreach ( $verifiers as $verifier  ) :?>
                <option value="<?= $verifier['id']?>"<?php
                    if(isset($letter)){
                        echo ($verifier['id'] == $letter['verifier']) ? 'selected' : '';
                    }
                    ?>><?= $verifier['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="VerifiedBy">Assign To</label>
        <select name="assign_to" id="assign_to" class="w-full py-2 px-2 rounded text-gray-600 text-sm" required>
            <?php foreach ( $assignees as $assignee  ) :?>
                <option value="<?= $assignee['id']?>"
                    <?php
                    if(isset($letter)) {
                        echo($assignee['id'] == $letter['assign_to']) ? 'selected' : '';
                    }
                    ?>><?= $assignee['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="Urgecy">Priority</label>
        <select name="priority" class="w-full py-2 px-2 rounded text-gray-600 text-sm" required>
            <?php foreach ( $priority as $prior ) :?>
                <option value="<?= $prior ?>"
                    <?php
                        if(isset($letter)) {
                            echo($prior == $letter['priority']) ? 'selected' : '';
                        }
                    ?>><?= $prior ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div class="mt-8">
    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="copy">Send a Copy to</label>
        <textarea class="w-full py-1 px-2 rounded-md text-gray-600 text-sm dhivehibas" name="copy" dir="rtl"><?php echo $letter['copy'] ?? null ?></textarea>
    </div>
    <div class="">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="Content">Content</label>
        <textarea class="w-full py-1 px-2 rounded-md text-gray-600 text-sm" rows="4" name="letter_content" dir="rtl"><?php echo $letter['letter_content'] ?? null ?></textarea>
    </div>
</div>

<div class="mt-4">
    <?php if((session()->get('level_id') == 3) ||(session()->get('level_id') == 2)): ?>
        <button class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center" name="status" type="submit" value="draft">
            <?= isset($letter) ? 'Update As Draft' : 'Save As Draft' ?>
        </button>
     <?php endif; ?>


     <?php if(isset($letter)): ?>
         <?php if(session()->get('level_id') == 2 && $letter['supervisor'] != 31 && $letter['supervisor'] == session()->get('userid') && $letter['status'] == 'pending' || $letter['status'] == 'verified'): ?>
            <button class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center" name="status" type="submit" value="changes">
                Save Changes
            </button>
         <?php endif; ?>
     <?php endif; ?>


     <?php if(isset($letter)): ?>
        <?php if(session()->get('verify_id') == 1 && $letter['verifier'] != 38 && $letter['verifier'] == session()->get('userid') && $letter['status'] == 'verified'): ?>
            <button class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center" name="status" type="submit" value="updatechanges">
                Update Changes
            </button>
        <?php endif; ?>
    <?php endif; ?>


     <?php if(session()->get('level_id') == 3): ?>
        <button class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center" id="super" name="status" type="submit" value="pending">
            Send to Supervisor
        </button>
    <?php endif; ?>



    <?php if(session()->get('level_id') == 3 || session()->get('level_id') == 2): ?>
        <button class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center" id="veri" name="status" type="submit" value="verified">
                Send to Verifier
        </button>
    <?php endif; ?>




    <?php if(session()->get('level_id') == 3 || session()->get('level_id') == 2): ?>
        <button class="bg-green-600 hover:bg-green-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center" id="appro" name="status" type="submit" value="proceed">
            Send For Approval
        </button>
    <?php endif; ?>
</div>



<script type="text/javascript">

    function supervise(){
        var supervisor = document.getElementById("supervisor");
        var verif = document.getElementById("verifier");

        if (supervisor.value == '31') {
            document.getElementById("super").style.display="none";
        }
        else if (supervisor.value == '31' && verif.value == '38') {
            document.getElementById("veri").style.display="none";
        } else {
             document.getElementById("super").style.display="";
        }
    }


    function verfier(){
        var supervisor = document.getElementById("verifier");
        if (supervisor.value == '38') {
            document.getElementById("veri").style.display="none";
        } else {
            document.getElementById("veri").style.display="";
        }
    }


    function approver() {
        var supervisor = document.getElementById("supervisor");
        var verif = document.getElementById("verifier");
        var approver = document.getElementById('appro').classList;

        if(supervisor.value == 31 && verif.value == 38){
            approver.remove("hidden");
        } else {

        }


    }


window.onload = function() {
    supervise();
    verfier();
    approver();
};



</script>