<div class="mt-10 mx-10"><!-- table -->
    <?= $this->include('letters/table/_header') ?>
    <?= $this->include('letters/table/_list') ?>
    <?= $this->include('letters/table/_footer') ?>

    <div class="float-right px-2 pb-5">
        <?php echo $pager->simplelinks('default','paging') ?>
    </div>

</div>