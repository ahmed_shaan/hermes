<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/print.css?b=06">

</head>
<body>
<!--<body onload="window.print()" onfocus="window.close()">-->
<div class="canvas_div_pdf">

<p class="bismmi dhivehibas">`</p>
<div class="page-header">
</div>
<div class="page-footer">
</div>
<table>
    <thead>
        <tr>
            <td>
                 <!--place holder for the fixed-position header-->
                 <div class="page-header-space"></div>
            </td>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <div class="page">
<!--                <div class="lettaer_second">-->
                <!-- <img src="/assets/img/letter/logo.png?l=004" alt="logo" class="office-logo pull-right" height="87px"/> -->

                <p class="dhivehibas letter-number"><span style="unicode-bidi: embed;"><?= $letter['letter_number']; ?> :ނަންބަރު</span></p>
<!--                </div>-->
                    <?php
                        if($letter['priority'] == 'Normal'){
                            echo '<img src="/assets/img/stamps/normal.png" alt="priority"  class="pri-stamp" width = "150px" />';
                        }
                        if($letter['priority'] == 'Urgent'){
                            echo '<img src="/assets/img/stamps/urgent.png" alt="priority"  class="pri-stamp" width = "150px" />';
                        }
                        if($letter['priority'] == 'Very Urgent'){
                            echo '<img src="/assets/img/stamps/vurgent.png" alt="priority" class="pri-stamp" width = "150px" />';
                        }
                        if($letter['priority'] == 'Confidential'){
                            echo '<img src="/assets/img/stamps/confidential.png" alt="priority" class="pri-stamp" width = "150px"/>';
                        }
                    ?>
                <img src="/assets/img/letter/stamp.png?b=04" alt="Stamp" class="stamp" width = "130px"/>
                <p class="dhivehibas initials"><?= $letter['initials']; ?></p>
                <p class="dhivehibas pagesNumber"></p>
                <p class="dhivehibas pagesNumber"></p>
                <div class="main" dir="rtl">
                    <div class="dhivehibas" style="margin-top: 20px;">
                        <?php
                            $content = $letter['letter_content'];
                            $replaceThis = array('<p>','<p dir="RTL">');
                            $displayContent = str_replace($replaceThis,'<p style="text-indent: 150px; text-align: justify; margin-top:0; margin-bottom:0;" class="text-sm text-gray-600 dhivehibas">',$content);
                            echo $displayContent;
                        ?>
                    </div>
                    <p class="dhivehibas" style="text-indent: 150px;margin:0;">އިޙްތިރާމް ގަބޫލުކުރެއްވުން އެދެމެވެ.</p>
                    <?php
                    $uCal = new \App\Libraries\uCal();
                    $uCal->setLang("dv");
                    $timestamp = strtotime($letter['created_at']);
                    $date = date('d-m-Y', $timestamp);
                    $meelaadhee = $uCal->date("d F Y", strtotime($date), 0);
                    $hijri = $uCal->date("d F Y", strtotime($date), 1);
                    ?>
                    <p class="dhivehibas" style="text-indent: 150px; margin-top: 0; margin-bottom:0;"><?= $hijri ?>ހ.</p>

                    <p class="dhivehibas" style="text-indent: 150px; margin-top: 0; margin-bottom:0;"><?= $meelaadhee ?>މ.</p>
                    <div class="approver">
                        <p class="dhivehibas haadhim" style="margin-top: 0; margin-bottom: 0">
                            <?php
                                if($letter['gender'] == 'Male'){
                                    echo "ހާދިމްކުމް";
                                } else {
                                    echo "ހާދިމަތުކުމް";
                                }
                            ?>
                        </p>
                        <?php
                            if($letter['status'] == 'approved'){
                                echo '<img src="data:image/png;base64,' . base64_encode($letter['signature']) . '"width = "150px" />';
                            } else {
                                echo '<div class="notSigned"> </div>';
                            }
                        ?>
                        <p class="dhivehibas" style="margin-top: 0; margin-bottom:0;"><?= $letter['dhiName']; ?></p>
                        <p class="dhivehibas" style="margin-top: 0; margin-bottom:0;"><?= $letter['dhiDesig']; ?></p>
                    </div>
                    <!-- Recipient Office and Island-->
                    <div class="reciepients">
                        <div class="footer-up">
                            <p class="dhivehibas" style="margin-bottom:0;"><?= $letter['recipient_office']; ?></p>
                            <p class="dhivehibas" style="margin-top: 0; margin-bottom:0;"><?= $letter['recipient_island']; ?></p>
                        </div>
                        <div class="row">
                            <div class="">
                                <?php
                                    if($letter['copy'] != ''){
                                        echo '<p class="dhivehibas" style="margin-top: 0; margin-bottom:0;">ކޮޕީ:</p>';
                                    }
                                ?>
                            </div>
                            <div class="">
                                <?php $paragraphs = explode("\n", $letter['copy']); ?>
                                <div class="">
                                    <?php for($i = 0; $i < count ($paragraphs); $i++): ?>
                                        <?php
                                            echo $paragraphs[$i] = '<p style="text-indent: 25px;margin-top: 0; margin-bottom:0;" class="dhivehibas">'. $paragraphs[$i] . '</p>';
                                        ?>
                                    <?php endfor;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>
                <!--place holder for the fixed-position footer-->
                <div class="page-footer-space">
                </div>
            </td>
        </tr>
    </tfoot>
</table>
</div>
</body>
</html>