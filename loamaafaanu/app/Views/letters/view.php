<?= $this->extend('layouts/main') ?>

<?= $this->section('content') ?>
    <div class="mt-20 mx-20 bg-white">
        <!-- Letter Details Card -->
        <div class="px-10 py-10 md:flex md:items-center md:justify-around md:py-5">
            <a href="<?= base_url('/letters') ?>" class="text-gray-600 text-xs hover:text-gray-700 font-medium rounded inline-flex items-center">
                <svg class="fill-current w-8 h-8" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.707-10.293a1 1 0 00-1.414-1.414l-3 3a1 1 0 000 1.414l3 3a1 1 0 001.414-1.414L9.414 11H13a1 1 0 100-2H9.414l1.293-1.293z" clip-rule="evenodd"></path></svg>
            </a>
            <p class="text-xs text-gray-600">Verified By: <strong><?= $letter['supervisorName'] ?></strong></p>
            <p class="text-xs text-gray-600">Approved By: <strong><?= $letter['assignedName'] ?></strong></p>
            <p class="text-xs text-gray-600">Reference Number: <strong><?= $letter['letter_reference'] ?></strong></p>
<!--            <a href="--><?//= base_url('lettersTemplate/' . $letter['id'])  ?><!--" target="_blank" class="float-right bg-teal-600 hover:bg-teal-700 text-white text-xs hover:text-white font-medium py-2 px-3 rounded inline-flex items-center">-->
            <a href="<?= base_url('lettersTemplate/' . $letter['id'])  ?>" target="_blank" class="hidden lg:float-right lg:bg-teal-600 hover:bg-teal-700 lg:text-white text-xs hover:text-white font-medium lg:py-2 lg:px-3 rounded lg:inline-flex lg:items-center">
                <svg viewBox="0 0 20 20" fill="currentColor" class="fill-current w-4 h-4 mr-2"><path fill-rule="evenodd" d="M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V7.414A2 2 0 0015.414 6L12 2.586A2 2 0 0010.586 2H6zm5 6a1 1 0 10-2 0v3.586l-1.293-1.293a1 1 0 10-1.414 1.414l3 3a1 1 0 001.414 0l3-3a1 1 0 00-1.414-1.414L11 11.586V8z" clip-rule="evenodd"></path></svg>
                <span>Download</span>
            </a>
        </div>
    </div>

    <div class="mt-5 mx-20 bg-white px-10 py-10">
            <div class="grid grid-cols md:grid md:grid-cols-3 md:gap-8">
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Subject</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?= $letter['subject'] ?></h3>
                </div>
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Letter Number</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?= $letter['letter_number'] ?></h3>
                </div>
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Priority</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?= $letter['priority'] ?></h3>
                </div>
            </div>
            <div class="grid grid-cols md:grid md:grid-cols-3 md:gap-8 mt-8">
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Written By</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?= $letter['name'] ?></h3>
                </div>
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Created Date</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?= $letter['created_at'] ?></h3>
                </div>
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Changed By Supervisor</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?php if(isset($letter['supervisorName'])) { echo $letter['supervisorName']; } else {'No Changes';}  ?></h3>
                </div>
            </div>
            <div class="grid grid-cols md:grid md:grid-cols-3 md:gap-8 mt-8">
                <div>
                     <h3 class="text-sm text-gray-700 py-2 font-bold">Changed By Verifier</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2"><?php  if(isset($letter['verifierName'])) { echo $letter['verifierName']; } else {'No Changes';}  ?></h3>
                </div>
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Recipient Office</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2 dhivehibas" dir="rtl"><?= $letter['recipient_office'] ?></h3>
                </div>
                <div>
                    <h3 class="text-sm text-gray-700 py-2 font-bold">Recipient Island</h3>
                    <hr>
                    <h3 class="text-sm text-gray-600 py-2 dhivehibas" dir="rtl"><?= $letter['recipient_island'] ?></h3>
                </div>
                
            </div>
    </div>
    <div class="hidden md:mt-5 md:mx-20 md:bg-white md:px-10 md:py-10">
            <div>
                <h3 class="hidden text-sm text-gray-700 py-2 font-bold">Content</h3>
                <hr>
                <div class="hidden px-20 py-5 dhivehibas text-justify" dir="rtl">
                    <?php $paragraphs = explode("\n", $letter['letter_content']); ?>
                    <div class="">
                        <?php for($i = 0; $i < count ($paragraphs); $i++): ?>
                            <?php
                            echo $paragraphs[$i] = '<p style="text-indent: 150px; text-align: justify;" class="text-sm text-gray-600 dhivehibas">'. $paragraphs[$i] . '</p>';
                            ?>
                        <?php endfor;?>
                    </div>
            </div>
    </div>


<?= $this->endSection() ?>