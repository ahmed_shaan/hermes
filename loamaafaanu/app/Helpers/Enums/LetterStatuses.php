<?php


namespace App\Helpers\Enums;


abstract class LetterStatuses

{
    use EnumsTrait;

    /*
    * Initialize labels
    */
    protected static function initLabels()
    {
        static::$labels = [
            'draft' => 'Draft',
            'pending' => 'Pending',
            'verified' => 'Verified',
            'proceed' => 'Proceed',
            'incomplete' => 'Incomplete',
            'approved' => 'Approved',
            'rejected' => 'Rejected'
        ];
    }

    public static function getSlug($key)
    {
        return $key;
    }
}