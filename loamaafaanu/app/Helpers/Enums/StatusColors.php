<?php

namespace App\Helpers\Enums;

class StatusColors {

//    private static $colors = [
//        'pending' => 'bg-orange-500',
//        'incomplete' => 'bg-red-500',
//        'verified' => 'bg-blue-500',
//        'approved'=>'bg-green-500',
//        'rejected' => 'bg-red-800',
//        'draft' => 'bg-teal-500'];

    private static $colors = [
        'pending' => 'bg-orange-600',
        'incomplete' => 'bg-red-600',
        'verified' => 'bg-blue-600',
        'proceed' => 'bg-blue-800',
        'approved'=>'bg-green-600',
        'rejected' => 'bg-red-800',
        'draft' => 'bg-teal-600'];

    public static function getColors()
    {
        return self::$colors;
    }
}