<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('UserController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('letters', 'LetterController::index',['filter' => 'login']);
$routes->get('letters/create','LetterController::create',['filter' => 'login']);
$routes->post('letters/store','LetterController::store',['filter' => 'login']);
$routes->get('letters/(:num)', 'LetterController::view/$1',['filter' => 'login']);
$routes->get('lettersTemplate/(:num)', 'LetterController::letterView/$1',['filter' => 'login']);
$routes->get('lettersTemplateIndividual/(:num)', 'LetterController::letterViewInd/$1',['filter' => 'login']);
$routes->get('letters/(:num)/edit', 'LetterController::edit/$1',['filter' => 'login']);
$routes->post('letters/(:num)/update', 'LetterController::update/$1',['filter' => 'login']);
$routes->post('letters/(:num)/status-update', 'LetterController::statusUpdate/$1',['filter' => 'login']);
$routes->get('letters/(:num)/delete', 'LetterController::delete/$1',['filter' => 'login']);
$routes->post('messages/(:num)/store', 'MessageController::store/$1',['filter' => 'login']);

$routes->get('profile/(:num)', 'UserController::userProfile/$1',['filter' => 'login']);

$routes->post('passChange', 'UserController::changePassword',['filter' => 'login']);
$routes->post('avatar', 'UserController::avatar',['filter' => 'login']);
$routes->get('profile/(:num)/remove', 'UserController::removeAvatar/$1',['filter' => 'login']);

$routes->get('pdfDownload/(:num)', 'LetterController::htmlToPDF/$1',['filter' => 'login']);

$routes->get('/', 'UserController::index',['filter' => 'noauth']);
$routes->get('logout', 'UserController::logout');

$routes->get('403', function(){
    return view('403');
});




/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */

if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
