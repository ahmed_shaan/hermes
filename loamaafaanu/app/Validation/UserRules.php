<?php

namespace App\Validation;

use App\Models\User;

class UserRules
{
    public function validateUser(string $str, string $fields, array $data)
    {
        $auth = new User();
        $user = $auth->where('nid', $data['nid'])->first();

        if(!$user)
            return false;

        return password_verify($data['password'], $user['password']);
    }
}