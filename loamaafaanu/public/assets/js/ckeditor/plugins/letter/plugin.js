CKEDITOR.plugins.add( 'letter', {
    icons: 'letter',
    init: function(editor) {
        editor.addCommand( 'insertNumber', {
            exec:function (editor) {
                let element = CKEDITOR.dom.element.createFromHtml( "<span style='unicode-bidi: embed !important;' dir='ltr'><span dir='rtl'>( )</span> <span dir='ltr'>221&#8209;CDP/333/2020/91</span></span>" );
                editor.insertElement(element);
            }
        });

        // Create the toolbar button that executes the above command.
        editor.ui.addButton( 'letter', {
            label: 'Insert Letter Number',
            command: 'insertNumber',
            toolbar: 'paragraph'
        });
    }
});